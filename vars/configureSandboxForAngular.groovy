def call(Map params) {
	def file = params.file
	def sandboxId = params.sandboxId
	def apiUrl = params.apiUrl ?: (env["ZP_${params.server.toUpperCase()}_SERVER_URL"] + '/zbo/pub/business')
	
	echo "Configure front to use sandbox ${sandboxId} on ${apiUrl}"
	def content = readFile(file)
	content = content.replaceAll("(apiUrl[^:]*:\\s*['\"]).+(['\"],?)", "\$1"+apiUrl+"\$2")
	content = content.replaceAll("(sandboxId[^:]*:\\s*['\"]).+(['\"],?)", "\$1"+sandboxId+"\$2")
	writeFile(file: file, text: content)
}